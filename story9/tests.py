from django.test import TestCase, Client

# Login Function Unit Test
class loginFunctionTests(TestCase):
    def test_url_register(self):
        resp = Client().get('/story9/register/')
        self.assertEquals(200, resp.status_code)
    
    def test_url_login(self):
        resp = Client().get('/story9/login/')
        self.assertEquals(200, resp.status_code)