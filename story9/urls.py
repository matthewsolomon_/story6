from django.urls import path

from . import views

app_name = 'story9'

urlpatterns = [
    path('register/', views.register, name='register'),
    path('login/', views.logging_in, name='logging_in'),
    path('logout/', views.logging_out, name='logging_out')
]