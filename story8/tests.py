from django.test import TestCase , Client
from django.urls import reverse, resolve
from story8.views import index as ix
from story8.views import livre as liv

class url_tests(TestCase):
    def testUrl(self):
        storys8_url = resolve(reverse("story8:index"))
        self.assertEquals(storys8_url.func, ix)
        
"""
//class views_tests(TestCase):
    def testViews(self):
        self.assertTemplateUsed(Client().get(reverse("story8:index")), "story8/index.html")

class index_tests(TestCase):
    def testIndex(self):
        response = self.client.get('/story8/')
        self.assertIn('La Bibliothèque de Monsieur Matthew', response.content.decode())/
"""
