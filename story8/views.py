from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
import urllib3
import json

#Story 8
@login_required(login_url="/story9/login")
def index(request):
    context = {
        "page" : "storys8",
        "namanya" : request.user.username
    }
    return render(request, "story8/index.html", context)

def livre(request):
    http = urllib3.PoolManager()
    response = http.request(
        "GET", "https://www.googleapis.com/books/v1/volumes?q=" + request.GET.get('q', ''))
    return JsonResponse(json.loads(response.data.decode('utf8')))