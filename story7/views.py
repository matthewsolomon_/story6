from django.shortcuts import render
from django.contrib.auth.models import User

#this is the views
def index(request):
    context = {
        "page" : "storys7",
        "namanya" : request.user.username
    }
    return render(request, "story7/index.html", context)