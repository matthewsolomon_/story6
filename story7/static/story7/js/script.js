$(document).ready(function(){
    $("#accordion").accordion();
    function sortByPosition(a, b) {
        var pos1 = Number($(a).attr("position"))
        var pos2 = Number($(b).attr("position"))
        
        if (pos1 < pos2) return -1;
        if (pos2 > pos1) return 1;
        return 0;
    }

    function sort_ul_up(clicked_h3, operation) {
        var elist = $("#accordion").children()
        var ul = $("#accordion")

        var clicked_pos = elist.index(clicked_h3)
        var clicked_div = elist[clicked_pos+1]
        if (clicked_pos == 0 && operation == "up") return
        if (clicked_pos == elist.length-1 && operation == "down") return

        

        if (operation == "up") {
            var above_h3 = $(elist[clicked_pos-2])
            var above_div = $(elist[clicked_pos-1])

            $(clicked_h3).attr("position", above_h3.attr("position")) 
            $(clicked_div).attr("position", above_div.attr("position"))

            above_h3.attr("position", above_h3.attr("position") + 1) 
            above_div.attr("position", above_div.attr("position") + 1)
        
        } else if (operation == "down") {
            var below_h3 = $(elist[clicked_pos+2])
            var below_div = $(elist[clicked_pos+3])

            $(clicked_h3).attr("position", below_h3.attr("position")) 
            $(clicked_div).attr("position", below_div.attr("position"))

            below_h3.attr("position", below_h3.attr("position") - 1)
            below_div.attr("position", below_div.attr("position") - 1)
        }

        elist.sort(sortByPosition)
       
        $.each(elist, (index, li) => {
            ul.append(li);
        })
    }

    $(".up").click((e) => {
        var las = e.target.parentElement
        console.log("up")
        sort_ul_up(las,"up")
    })

    $(".down").click((e) => {
        var las = e.target.parentElement
        sort_ul_up(las,"down")
    })
})