from django.test import TestCase , Client
from django.urls import reverse, resolve
from story7.views import index as ix

class url_tests(TestCase):
    def testUrl(self):
        storys7_url = resolve(reverse("story7:index"))
        self.assertEquals(storys7_url.func, ix)
        
class views_tests(TestCase):
    def testviews(self):
        self.assertTemplateUsed(Client().get(reverse("story7:index")), "story7/index.html")

class index_tests(TestCase):
    def testindex(self):
        response = self.client.get('/')
        self.assertIn('Matthew T.P. Solomon', response.content.decode())